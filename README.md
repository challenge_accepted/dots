# Dots

# Void Linux
Dotfiles of my latest Void Linux setup

NOTE: These dots are intended for my setup. To make them work for your machine, you might have to modify some.
As most of these dots are not my own, you will find a link to the original owner if applicable

## Info
- OS: [Void Linux](https://voidlinux.org/)  
- WM: [DWM](https://dwm.suckless.org/)
- File manager: Ranger
- Editor: [Code](https://github.com/microsoft/vscode) and Neovim
- Office Suite: Libreoffice
- Fetch: Neofetch
- Colorscheme: [Nord](https://www.nordtheme.com/) with extra dark color #242831

## Current Screenshots
Pending
